<?php
/*
Template Name: קטגוריות
*/

get_header();
$fields = get_fields();
$terms = get_terms([
		'taxonomy' => 'project_cat',
		'parent' => 0,
		'hide_empty' => false,
]);
?>

<article class="article-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12 mb-5">
				<div class="base-output text-center">
					<h1 class="block-title mb-1"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="padding-no">
		<?php if ($terms) : ?>
			<div class="posts-output">
				<div class="container">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($terms as $i => $cat) : ?>
							<div class="col-lg-3 col-md-6 col-sm-10 col-11 min-padding mb-4 wow fadeInUp"
								 data-wow-delay="0.<?= $i * 2; ?>s">
								<?php get_template_part('views/partials/card', 'category', [
										'cat' => $cat,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="form-line-none">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($more_posts = $fields['page_pro_posts']) {
	get_template_part('views/partials/content', 'posts_three',
		[
			'posts_title' => $fields['page_pro_posts_title'],
			'posts' => $more_posts,
		]);
}
if ($slider = $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $slider,
		]);
}
get_footer(); ?>
