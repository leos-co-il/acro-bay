<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 6,
	'post_type' => 'post',
]);
$count_posts = wp_count_posts();
if ( $count_posts ) {
	$published_posts = $count_posts->publish;
}
?>

<article class="article-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12 mb-5">
				<div class="base-output text-center">
					<h1 class="block-title mb-1"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="padding-no">
		<?php if ($posts->have_posts()) : ?>
			<div class="posts-output">
				<div class="container">
					<div class="row justify-content-center align-items-stretch vacas">
						<?php foreach ($posts->posts as $i => $post) : ?>
							<?php get_template_part('views/partials/card', 'post_ajax', [
								'post' => $post,
							]); ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($published_posts && $published_posts > 6) : ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<div class="post-link more-link" data-type="post">
					טען עוד
				</div>
			</div>
		</div>
	</div>
	<?php endif; ?>
</article>
<div class="form-line-none">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($more_posts = $fields['page_pro_posts']) {
	get_template_part('views/partials/content', 'posts_three',
		[
			'posts_title' => $fields['page_pro_posts_title'],
			'posts' => $more_posts,
		]);
}
if ($slider = $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $slider,
		]);
}
get_footer(); ?>
