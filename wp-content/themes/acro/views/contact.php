<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
$tel_more = opt('tel_more');
$map = opt('map_image');
?>
<article class="contact-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12 mb-5">
				<div class="base-output text-center">
					<h1 class="block-title mb-1"><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-12 mt-2">
				<div class="row justify-content-center align-items-start mb-5">
					<div class="col-xl-4 col-lg-6 col-md-10 col-11 contact-col">
						<?php if ($tel) : ?>
							<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</div>
								<div class="contact-info">
									<h3 class="contact-type contact-type-title not-hover">דברו איתנו</h3>
									<p class="contact-type"><?= $tel; ?></p>
								</div>
							</a>
						<?php endif; ?>
						<?php if ($mail) : ?>
							<a href="mailto:<?= $mail; ?>" class="contact-item wow flipInX" data-wow-delay="0.4s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-mail.png">
								</div>
								<div class="contact-info">
									<h3 class="contact-type contact-type-title">כתבו לנו</h3>
									<p class="contact-type"><?= $mail; ?></p>
								</div>
							</a>
						<?php endif; ?>
						<?php if ($tel_more) : ?>
							<a href="tel:<?= $tel_more; ?>" class="contact-item wow flipInX" data-wow-delay="0.6s">
								<div class="contact-icon-wrap">
									<img src="<?= ICONS ?>contact-tel.png">
								</div>
								<div class="contact-info">
									<h3 class="contact-type contact-type-title not-hover">טלפון נוסף</h3>
									<p class="contact-type"><?= $tel_more; ?></p>
								</div>
							</a>
						<?php endif; ?>
					</div>
					<div class="col-xl-4 col-lg-6 col-12">
						<div class="contact-page-form dark-submit">
							<?php if ($contact_form_title = $fields['contact_form_title']) : ?>
								<h3 class="form-text mb-3">
									<?= $contact_form_title; ?>
								</h3>
							<?php endif; ?>
							<?php getForm('9'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($map) : ?>
		<div class="map-image">
			<?php if ($address) : ?>
				<div class="container">
					<div class="row justify-content-center">
						<div class="col mb-2">
							<div class="contact-info d-flex justify-content-center align-items-center">
								<span class="contact-type contact-type-title not-hover">בקרו אותנו ב-</span>
								<span class="contact-type" href="https://www.waze.com/ul?q=<?= $address; ?>" target="_blank">
								<?= $address; ?>
							</span>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
			<img src="<?= $map['url']; ?>">
		</div>
	<?php endif; ?>
</article>
<?php get_footer(); ?>
