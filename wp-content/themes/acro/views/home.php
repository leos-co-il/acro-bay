<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>

<section class="home-main"
	<?php if ($home_back = $fields['home_back']) : ?>
		style="background-image: url('<?= $home_back['url']; ?>')"
	<?php endif; ?>>
	<div class="home-overlay">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row">
						<div class="col-xl-6 col-lg-8 col-md-10 col-12">
							<?php if ($h_form_title = $fields['home_form_title']) : ?>
								<h2 class="form-title white-text"><?= $h_form_title; ?></h2>
							<?php endif;
							if ($h_form_text = $fields['home_form_text']) : ?>
								<p class="form-text white-text"><?= $h_form_text; ?></p>
							<?php endif;
							getForm('6');?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<img src="<?= IMG ?>home-bottom.png" class="bottom-line">
</section>
<?php if ($about = $fields['h_about_text']) : ?>
<section class="home-about-block">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-12">
				<div class="row justify-content-between align-items-center">
					<div class="col-lg-6 col-md-9 col-12 d-flex flex-column align-items-start">
						<div class="base-output mb-3">
							<?= $about; ?>
						</div>
						<?php if ($home_about_link = $fields['h_about_link']) : ?>
							<a href="<?= $home_about_link['url'];?>" class="post-link">
								<?php
								echo isset($home_about_link['title']) ? $home_about_link['title'] : 'עוד עלינו';
								?>
							</a>
						<?php endif; ?>
					</div>
					<?php if($logo = opt('logo')) : ?>
						<div class="col-lg-5 col-md-3 col logo-col">
							<a class="d-flex justify-content-center align-items-center" href="/">
								<img src="<?= $logo['url']; ?>">
							</a>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php
endif;
if ($home_cats = $fields['home_cats']) : ?>
	<section class="posts-output pt-4">
		<div class="container">
			<?php if ($home_posts_title = $fields['home_cats_title']) : ?>
				<div class="row justify-content-center">
					<div class="col">
						<h2 class="block-title"><?= $home_posts_title; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($home_cats as $i => $cat) : ?>
					<div class="col-lg-3 col-md-6 col-sm-10 col-11 min-padding mb-4 wow fadeInUp"
					data-wow-delay="0.<?= $i * 2; ?>s">
						<?php get_template_part('views/partials/card', 'category', [
								'cat' => $cat,
						]); ?>
					</div>
				<?php endforeach; ?>
			</div>
			<?php if ($home_cats_link = $fields['home_cats_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a class="transparent-link" href="<?= $home_cats_link['url']; ?>">
							<?php
							echo isset($home_cats_link['title']) ? $home_cats_link['title'] : 'לכל הקטגוריות';
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($benefits = $fields['h_why_item']) : ?>
	<section class="why-us-block">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<?php if ($home_why_title = $fields['h_why_title']) : ?>
						<div class="row justify-content-center">
							<div class="col">
								<h2 class="block-title"><?= $home_why_title; ?></h2>
							</div>
						</div>
					<?php endif; ?>
					<div class="row justify-content-center align-items-start">
						<?php foreach ($benefits as $key => $benefit) : ?>
							<div class="col-md-3 col-6 benefit-col mb-3">
								<div class="benefit-item wow zoomIn" data-wow-delay="0.<?= $key * 2; ?>s">
									<div class="benefit-icon">
										<?php if ($benefit_icon = $benefit['why_icon']) : ?>
											<img src="<?= $benefit_icon['url']; ?>">
										<?php endif; ?>
									</div>
									<h3 class="middle-title">
										<?= $benefit['why_title']; ?>
									</h3>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
<div class="projects-output">
	<?php if ($pro_first = $fields['home_projects_first']) : ?>
		<div class="project-card first-pro-card">
			<div class="post-img project-img" <?php if ($pro_img = $pro_first['pr_item_back']) : ?>
				style="background-image: url('<?= $pro_img['url']; ?>')"
			<?php endif; ?>>
				<div class="d-flex flex-column justify-content-center align-items-center card-div">
					<div class="base-output"><?= $pro_first['pr_item_text']; ?></div>
					<?php if ($pro_link = $pro_first['pr_item_link']) : ?>
						<a class="inverse-link" href="<?= $pro_link['url']; ?>">
							<?php echo isset($pro_link['title']) ? $pro_link['title'] : 'לכל הפרוייקטים'; ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<?php if ($projects = $fields['home_projects']) : foreach ($projects as $key => $project) :
		get_template_part('views/partials/card', 'project', [
				'project' => $project,
		]);
		endforeach; endif; ?>
</div>
<?php get_template_part('views/partials/repeat', 'form');
if ($slider = $fields['home_slider_seo']) : ?>
	<div class="reverse-slider">
		<?php
		get_template_part('views/partials/content', 'slider',
				[
						'img' => $fields['home_slider_img'],
						'content' => $slider,
				]);
		?>
	</div>
<?php endif;
if ($home_posts = $fields['home_posts']) {
	get_template_part('views/partials/content', 'posts_three',
			[
					'posts_title' => $fields['home_posts_title'],
					'posts' => $home_posts,
					'posts_link' => $fields['home_posts_link'],
			]);
}
if ($slider = $fields['single_slider_seo']) {
			get_template_part('views/partials/content', 'slider',
				[
						'img' => $fields['slider_img'],
						'content' => $slider,
				]);
}
get_footer(); ?>
