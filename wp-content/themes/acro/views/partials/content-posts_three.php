<?php if (isset($args['posts']) && $args['posts']) : $home_posts = $args['posts']; ?>
	<div class="posts-output">
		<div class="container">
			<?php if ($home_posts_title = isset($args['posts_title']) ? $args['posts_title'] : '') : ?>
				<div class="row justify-content-center">
					<div class="col">
						<h2 class="block-title"><?= $home_posts_title; ?></h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($home_posts as $i => $post) : ?>
							<div class="col-lg-4 col-md-10 col-11 min-padding mb-4 wow zoomInUp" data-wow-delay="0.<?= $i * 2; ?>s">
								<?php get_template_part('views/partials/card', 'post', [
										'post' => $post,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
					<?php if ($home_posts_link = isset($args['posts_link']) ? $args['posts_link'] : '') : ?>
						<div class="row justify-content-end">
							<div class="col-auto">
								<a class="transparent-link" href="<?= $home_posts_link['url']; ?>">
									<?php
									echo isset($home_posts_link['title']) ? $home_posts_link['title'] : 'לכל המאמרים';
									?>
								</a>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
