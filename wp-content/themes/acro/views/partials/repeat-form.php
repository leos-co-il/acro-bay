<div class="repeat-form-block dark-submit">
	<div class="form-block-back">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-6 col-lg-8 col-md-10 col-12">
					<div class="post-form-wrap">
						<?php if ($logo = opt('logo')) : ?>
							<div class="logo mb-4">
								<img src="<?= $logo['url']; ?>">
							</div>
						<?php endif;
						if ($f_title = opt('repeat_form_title')) : ?>
							<h2 class="form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_text = opt('repeat_form_text')) : ?>
							<p class="form-text"><?= $f_text; ?></p>
						<?php endif; ?>
						<div class="wow zoomIn">
							<?php getForm('8');?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<img src="<?= IMG ?>form-bottom.png" class="form-bottom">
</div>
