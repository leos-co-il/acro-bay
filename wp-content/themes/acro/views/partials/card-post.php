<?php if (isset($args['post'])) : $post_item_current = $args['post'];
	$link = get_the_permalink($post_item_current); ?>
	<div class="post-card">
		<div class="post-card-column">
			<a class="post-img" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($post_item_current)) : ?>
					style="background-image: url('<?= postThumb($post_item_current); ?>')"
				<?php endif; ?>>
			</a>
			<div class="post-card-content">
				<h3 class="post-card-title"><?= $post_item_current->post_title; ?></h3>
				<p class="base-text mb-3">
					<?= text_preview($post_item_current->post_content, 15); ?>
				</p>
				<a class="post-link" href="<?= $link; ?>">
					קרא עוד
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
