<?php if (isset($args['project'])) : $post_item_current = $args['project'];
	$link = get_the_permalink($post_item_current); ?>
	<div class="project-card more-card" data-id="<?= $post_item_current->ID; ?>">
		<div class="post-img project-img" <?php if (has_post_thumbnail($post_item_current)) : ?>
			style="background-image: url('<?= postThumb($post_item_current); ?>')"
		<?php endif; ?>>
			<div class="project-card-overlay">
				<div class="d-flex flex-column justify-content-center align-items-center h-100 card-div">
					<h3 class="project-card-title"><?= $post_item_current->post_title; ?></h3>
					<a class="inverse-link" href="<?= $link; ?>">
						מעבר לפרוייקט
					</a>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
