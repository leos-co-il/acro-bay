<?php if (isset($args['cat'])) : $post_item_current = $args['cat'];
	$link = get_term_link($post_item_current); ?>
	<div class="post-card cat-card">
		<div class="post-card-column">
			<a class="post-img" href="<?= $link; ?>"
				<?php if ($img = get_field('cat_image', $post_item_current)) : ?>
					style="background-image: url('<?= $img['url']; ?>')"
				<?php endif; ?>>
			</a>
			<div class="post-card-content">
				<h3 class="post-card-title"><?= $post_item_current->name; ?></h3>
				<p class="base-text mb-3">
					<?= text_preview($post_item_current->description, 15); ?>
				</p>
				<a class="post-link" href="<?= $link; ?>">
					קרא עוד
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
