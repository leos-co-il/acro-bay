<?php if (isset($args['content']) && ($args['content'])) :
	$slider_img = isset($args['img']) ? ($args['img']) : ''; ?>
	<div class="base-slider-block arrows-slider">
		<div class="<?php echo $slider_img ? 'col-xl-5 col-lg-6 col-12' : 'col-12'; ?> slider-col-content">
			<div class="base-slider" dir="rtl">
				<?php foreach ($args['content'] as $content) : ?>
					<div class="slider-base-item">
						<div class="base-output">
							<?= $content['content']; ?>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>		<?php if ($slider_img) : ?>
			<div class="slider-img-col">
				<img src="<?= $slider_img['url']; ?>">
			</div>
		<?php endif; ?>
	</div>
<?php endif; ?>
