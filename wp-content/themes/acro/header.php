<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="container position-relative">
		<div class="drop-menu">
			<nav class="drop-nav">
				<?php getMenu('header-menu', '2', '', ''); ?>
			</nav>
		</div>
		<div class="row justify-content-between align-items-center">
			<div class="col-xl-5 col-sm col-3 d-flex justify-content-start align-items-center">
				<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
				<?php if ($file = opt('main_catalog')) : ?>
					<a class="post-link download-link hidden-file mr-3" download href="<?= $file['url']; ?>">
						הורדת הקטלוג
					</a>
				<?php endif; ?>
			</div>
			<?php if ($logo = opt('logo')) : ?>
				<div class="col-xl col-sm-4 col-6">
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="logo">
					</a>
				</div>
			<?php endif; ?>
			<div class="col-xl-5 col-sm col-3 tel-col-header">
				<?php if ($file = opt('main_catalog')) : ?>
					<a class="post-link download-link mr-3 header-file" download href="<?= $file['url']; ?>">
						הורדת הקטלוג
					</a>
				<?php endif; ?>
				<?php if ($tel = opt('tel')) : ?>
					<a href="tel:<?= $tel; ?>" class="header-tel d-flex justify-content-center align-items-center">
						<span class="tel-number">
							<?= $tel; ?>
						</span>
						<?= svg_simple(ICONS.'header-tel.svg'); ?>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</header>
<div class="fixed-triggers">
	<?php if ($whatsapp = opt('whatsapp')) : ?>
		<a href="<?= $whatsapp; ?>" class="whatsapp-fix">
			<img src="<?= ICONS ?>whatsapp.png">
		</a>
	<?php endif; ?>
	<div class="trigger-back pop-trigger">
		<img src="<?= ICONS ?>pop-trigger.png">
	</div>
</div>
<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form styled-form dark-submit">
					<span class="close-form">
						<img src="<?= ICONS ?>close.png">
					</span>
					<div class="post-form-wrap">
						<?php if ($logo) : ?>
							<div class="logo mb-4">
								<img src="<?= $logo['url']; ?>">
							</div>
						<?php endif;
						if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="form-title"><?= $f_title; ?></h2>
						<?php endif;
						if ($f_text = opt('pop_form_text')) : ?>
							<p class="form-text"><?= $f_text; ?></p>
						<?php endif;
						getForm('8'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
