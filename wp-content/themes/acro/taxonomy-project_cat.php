<?php

get_header();
$term = get_queried_object();
$children = get_terms( $term->taxonomy, [
	'parent'    => $term->term_id,
	'hide_empty' => false
]);
$posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'project',
	    'tax_query' => [
	    		[
            'taxonomy' => 'project_cat',
            'field' => 'term_id',
            'terms' => $term->term_id,
        ]
    ]
]);
?>
<article class="article-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12 mb-5">
				<div class="base-output text-center">
					<h1 class="block-title mb-1"><?= $term->name; ?></h1>
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($children) : ?>
	<div class="padding-no">
		<div class="posts-output">
			<div class="container">
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($children as $i => $cat) : ?>
						<div class="col-lg-3 col-md-6 col-sm-10 col-11 min-padding mb-4 wow fadeInUp"
							 data-wow-delay="0.<?= $i * 2; ?>s">
							<?php get_template_part('views/partials/card', 'category', [
									'cat' => $cat,
							]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>
	<?php else : ?>
		<div class="cats-list pb-5">
			<div class="container">
				<div class="row justify-content-center">
					<div class="<?php echo $posts ? 'col-lg-3 col-md-4 col-11' : 'col-lg-4 col-sm'; ?> cats-col-menu">
						<?php getMenu('sidebar-menu', '1', 'sidebar-wrap'); ?>
					</div>
					<?php if ($posts) : ?>
						<div class="col-lg-9 col-sm-8 col-11">
							<div class="row justify-content-center align-items-stretch">
								<?php foreach ($posts as $x => $post) : ?>
									<div class="col-xl-4 col-lg-6 col-12 mb-4 wow fadeInUp" data-wow-delay="0.<?= $x * 2; ?>s">
										<?php get_template_part('views/partials/card', 'post', [
												'post' => $post,
										]); ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					<?php else: ?>
						<div class="col-lg-9 col-md-8 col-11">
							<div class="row justify-content-center">
								<div class="col">
									<h3 class="block-title text-center">אין פרויקטים בקטגוריה</h3>
								</div>
							</div>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<div class="form-line-none">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($more_posts = get_field('page_pro_posts', $term)) {
	get_template_part('views/partials/content', 'posts_three',
		[
			'posts_title' => get_field('page_pro_posts_title', $term),
			'posts' => $more_posts,
		]);
}
if ($slider = get_field('single_slider_seo', $term)) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => get_field('slider_img', $term),
			'content' => $slider,
		]);
}
get_footer(); ?>
