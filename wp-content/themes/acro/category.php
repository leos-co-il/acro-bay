<?php

get_header();
$term = get_queried_object();
$posts = get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $term->term_id,
		]
	]
]);
?>
<article class="article-page-body page-body">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12 mb-5">
				<div class="base-output text-center">
					<h1 class="block-title mb-1"><?= $term->name; ?></h1>
					<?= category_description(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="padding-no">
		<?php if ($posts) : ?>
			<div class="posts-output">
				<div class="container">
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($posts as $i => $post) : ?>
							<div class="col-lg-4 col-md-6 col-sm-10 col-12 wow fadeIn mb-4" data-wow-delay="0.<?= $i; ?>s">
								<?php get_template_part('views/partials/card', 'post', [
									'post' => $post,
								]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<div class="form-line-none">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
if ($more_posts = get_field('page_pro_posts', $term)) {
	get_template_part('views/partials/content', 'posts_three',
		[
			'posts_title' => get_field('page_pro_posts_title', $term),
			'posts' => $more_posts,
		]);
}
if ($slider = get_field('single_slider_seo', $term)) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => get_field('slider_img', $term),
			'content' => $slider,
		]);
}
get_footer(); ?>
