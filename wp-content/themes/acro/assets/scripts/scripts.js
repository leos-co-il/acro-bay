(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	$( document ).ready(function() {
		var menu = $( '.drop-menu' );
		$('header .hamburger').click(function () {
			menu.slideFadeToggle();
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		$('.play-button').click(function() {
			var id = $(this).data('video');
			var frame = '<iframe width="100%" height="500px" src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('#iframe-wrapper').html(frame);
			$('#modalCenter').modal('show');
		});
		$('#modalCenter').on('hidden.bs.modal', function (e) {
			$('#iframe-wrapper').html('');
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
		});

		//Images at project page
		$('.change-img').click(function(){
			var src = $(this).data('src');
			$('#picture-main').attr('src', src);
		});
		//Hover menu
		$('.drop-nav .menu-item-has-children').hover(
			function() {
				$(this).children('.sub-menu').toggleClass('show');
			});
	});
	//More posts
	$('.more-link').click(function() {
		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});

		var type = $(this).data('type');
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			type: 'post',
			dataType: 'json',
			data: {
				type: type,
				ids: ids,
				action: 'get_more_function',
			},
			success: function (data) {
				if (!data.html) {
					$('.more-link').addClass('hide-more');
				}
				$('.vacas').append(data.html);
			}
		});
	});

})( jQuery );
