<?php

require THEMEPATH . '/vendor/autoload.php';

use DebugBar\DataCollector\ConfigCollector;
use DebugBar\StandardDebugBar;
global $post;
global $wpdb;
global $wp;



$debugbar = new StandardDebugBar();
$debugbarRenderer = $debugbar->getJavascriptRenderer();



$debugbar->addCollector(new DebugBar\DataCollector\ConfigCollector((array)$post, 'Post'));

$postID = get_the_ID();
$object_fields = get_fields($postID);

if($object_fields){
    $debugbar->addCollector(new DebugBar\DataCollector\ConfigCollector($object_fields, 'ACF'));
}

$seo = [
    'host' => get_site_url()
];

$checkForFiles = array('robots.txt','sitemap.xml');

$host = get_site_url();

foreach($checkForFiles as $file){
    $url = $host . '/' . $file;

    $ch = curl_init ($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);

    $output = curl_exec ($ch);

    if(curl_getinfo($ch)['http_code'] != 200){
        $seo[$file] = '['.$url.']Not Exist';
    }else{
        $seo[$file] = 'Does exist on <a target="_blank" href="'.$host . $file.'">' . $host . $file .'</a>' . '<br>';
    }

    curl_close($ch);

}


function indexed($url) {
    $url = 'http://webcache.googleusercontent.com/search?q=cache:' . urlencode($url);

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
    curl_setopt($ch, CURLOPT_NOBODY, true);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Chrome 10');

    if (!curl_exec($ch)) {
        // var_dump('failed');
        return false;
    }

    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    // var_dump($code);
    return $code == '200';
}





$debugbar->addCollector(new DebugBar\DataCollector\ConfigCollector((array)$wpdb, 'WPDB'));
$debugbar->addCollector(new DebugBar\DataCollector\ConfigCollector((array)$wp, 'WP'));
$debugbar->addCollector(new DebugBar\DataCollector\ConfigCollector([], 'SEO Local'));
$debugbar->addCollector(new DebugBar\DataCollector\ConfigCollector($seo, 'SEO Global'));


echo $debugbarRenderer->renderHead();
echo $debugbarRenderer->render();