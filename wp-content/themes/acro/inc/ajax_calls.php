<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}

add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$post_type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : 'post';
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$ids = explode(',', $ids_string);
	$query = new WP_Query([
		'post_type' => $post_type,
		'posts_per_page' => 3,
		'post__not_in' => $ids,
	]);
	$html = '';
	$result['html'] = '';
	foreach ($query->posts as $x => $item) {
		if ($post_type != 'post') {
			$html = load_template_part('views/partials/card', 'project', [
				'project' => $item,
			]);
			$result['html'] .= $html;
		} else {
			$html = load_template_part('views/partials/card', 'post_ajax', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
	}

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
