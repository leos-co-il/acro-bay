<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$currentType = get_post_type($postId);
$taxname = '';
switch ($currentType) {
	case 'project':
		$taxname = 'project_cat';
		$sameTitle = 'פרויקטים';
		break;
	case 'post':
		$taxname = 'category';
		$sameTitle = 'מאמרים';
		break;
}
$post_terms = wp_get_object_terms($postId, $taxname, ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 3,
	'post_type' => $currentType,
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => $taxname,
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['page_pro_posts']) {
	$samePosts = $fields['page_pro_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => $currentType,
		'post__not_in' => array($postId),
	]);
}
if ($fields['page_pro_posts_title']) {
	$sameTitle = $fields['page_pro_posts_title'];
}
$post_gallery = $fields['post_gallery'];
?>
<article class="article-page-body page-body body-back">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-11 col-12">
				<h1 class="block-title text-right mb-3"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-6 <?php echo $post_gallery ? 'col-md-8' : 'col-md-11'?> col-12 d-flex flex-column align-items-start">
				<div class="base-output base-post-output">
					<?php the_content(); ?>
				</div>
				<?php if ($file_1 = $fields['save_cat'] ? opt('main_catalog') : '') : ?>
					<a class="post-link download-link" download href="<?= $file_1['url']; ?>">
						הורדת הקטלוג
					</a>
				<?php endif;
				if ($more_text = $fields['post_more_content']) : ?>
					<div class="base-output base-post-output">
						<?= $more_text; ?>
					</div>
				<?php endif;
				if ($file_2 = $fields['save_file_post']) : ?>
					<a class="link-post-save" download href="<?= $file_1['url']; ?>">
						<img src="<?= ICONS ?>save-pdf.png">
						<span><?php echo isset($file_2['title']) ? $file_2['title'] : 'הורידו את הקובץ'?></span>
					</a>
				<?php endif; ?>
			</div>
			<?php if ($post_gallery) : ?>
				<div class="col-lg-6 col-md-4 col-sm-10 col-11 order-first-point">
					<a class="main-picture" href="<?php echo has_post_thumbnail() ? postThumb() : ''; ?>" data-lightbox="images">
						<img src="<?php echo has_post_thumbnail() ? postThumb() : ''; ?>" id="picture-main">
					</a>
					<div class="pictures-single">
						<?php foreach ($post_gallery as $img) : ?>
							<div class="change-img" data-src="<?= $img['url']; ?>"
							style="background-image: url('<?= $img['url']; ?>')"></div>
						<?php endforeach; ?>
					</div>
				</div>
			<?php else: ?>
				<div class="col-lg-6 col-md-10 sticky-form">
					<div class="styled-form post-page-form dark-submit">
						<div class="post-form-wrap">
							<?php if ($logo = opt('logo')) : ?>
								<div class="logo mb-4">
									<img src="<?= $logo['url']; ?>">
								</div>
							<?php endif;
							if ($f_title = opt('single_form_title')) : ?>
								<h2 class="form-title"><?= $f_title; ?></h2>
							<?php endif;
							if ($f_text = opt('single_form_text')) : ?>
								<p class="form-text"><?= $f_text; ?></p>
							<?php endif;
							getForm('8'); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>


<?php if ($post_gallery) : ?>
<div class="form-line-none">
	<?php get_template_part('views/partials/repeat', 'form'); ?>
</div>
<?php
endif;
if ($samePosts) {
	get_template_part('views/partials/content', 'posts_three',
		[
			'posts_title' => $sameTitle,
			'posts' => $samePosts,
		]);
}
if ($slider = $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
		[
			'img' => $fields['slider_img'],
			'content' => $slider,
		]);
}
get_footer(); ?>
