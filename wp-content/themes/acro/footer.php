<?php

$tel = opt('tel');
$mail = opt('mail');
$facebook = opt('facebook');
$address = opt('address');
$current_id = get_the_ID();
$contact_id = getPageByTemplate('views/contact.php');
?>
<footer>
	<div class="footer-main <?php echo ($current_id !== $contact_id) ? '' : 'mt-0'; ?>">
		<?php if ($current_id !== $contact_id) : ?>
			<img src="<?= IMG ?>footer-line.png" class="footer-line">
		<?php endif; ?>
		<div class="foo-form-block">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xl-6 col-lg-8 col-md-10 col-12">
						<div class="post-form-wrap">
							<?php if ($logo_white = opt('logo_white')) : ?>
								<div class="logo mb-4">
									<img src="<?= $logo_white['url']; ?>">
								</div>
							<?php endif;
							if ($f_title = opt('foo_form_title')) : ?>
								<h2 class="form-title"><?= $f_title; ?></h2>
							<?php endif;
							if ($f_text = opt('foo_form_text')) : ?>
								<p class="form-text white-text"><?= $f_text; ?></p>
							<?php endif;
							getForm('8'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-grad-back">
			<a id="go-top">
				<img src="<?= ICONS ?>top-arrow.png">
				<h4 class="to-top-text">חזרה למעלה</h4>
			</a>
			<div class="container footer-container-menu">
				<div class="row justify-content-between align-items-start">
					<div class="col-xl-3 col-lg-auto col-md-6 col-12 foo-menu">
						<h3 class="foo-title">
							תפריט ניווט
						</h3>
						<div class="menu-border-top">
							<?php getMenu('footer-menu', '2'); ?>
						</div>
					</div>
					<div class="col-lg col-md-6 col-12 foo-menu">
						<?php if ($foo_l_title = opt('foo_menu_title')) : ?>
							<h3 class="foo-title">
								<?php echo $foo_l_title ? $foo_l_title : 'קישורים'; ?>
							</h3>
						<?php endif; ?>
						<div class="menu-border-top">
							<?php getMenu('footer–two-menu', '2', 'hop-hey two-columns'); ?>
						</div>
					</div>
					<div class="col-lg-auto col foo-menu contacts-footer-menu">
						<h3 class="foo-title">
							נהיה בקשר
						</h3>
						<div class="menu-border-top">
							<ul class="contact-list d-flex flex-column">
								<?php if ($tel) : ?>
									<li>
										<a href="tel:<?= $tel; ?>" class="contact-info-footer">
											<img src="<?= ICONS ?>foo-tel.svg"><?= $tel; ?>
										</a>
									</li>
								<?php endif; ?>
								<?php if ($mail) : ?>
									<li>
										<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
											<img src="<?= ICONS ?>foo-mail.svg"><?= $mail; ?>
										</a>
									</li>
								<?php endif; ?>
								<?php if ($address) : ?>
									<li>
										<a href="https://waze.com/ul?q=<?= $address; ?>"
										   class="contact-info-footer" target="_blank">
											<img src="<?= ICONS ?>foo-geo.png"><?= $address; ?>
										</a>
									</li>
								<?php endif; ?>
							</ul>
						</div>
						<?php if ($facebook) : ?>
							<div class="facebook-widget">
								<iframe src="https://www.facebook.com/plugins/page.php?href=<?= $facebook; ?>&tabs=timeline&width=300px&height=200px&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=false&appId"
										width="250px" height="150px" style="border:none;overflow:hidden" scrolling="no"
										frameborder="0" allowTransparency="true" allow="encrypted-media">
								</iframe>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>


<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
